<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 
//������ ����� ���� �� ����� ���

class OwnPostRule extends Rule
{
	//����� ����� ����� �� �� ����
	
	public $name = 'OwnPostRule';
	
	//����� ��� ����� ��������, ���� ����� ���� ��� ���� ����,������ �� ���� �� ������� ���� ������ ������ ���� ����
	
	public function execute($user, $item, $params)
	{
		//����� ������ ���� - login
		
		if (!Yii::$app->user->isGuest) {
			//����� ������ ���� ����� owner
			
			return isset($params['user']) ? $params['user']->id == $user : false;
		}
		return false;
	}
}
?>