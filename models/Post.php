<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;



/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $category
 * @property string $author
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
	
	   public function rules()
    {
		
		
		$rules = []; 
		$stringItems = [['title','body','category','author','status'], 'string'];
		$integerItems  = ['created_at', 'updated_at', 'created_by', 'updated_by'];		
		if (\Yii::$app->user->can('assignPost')) {
			$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
	//	$ShortStringItems = [['name', 'email', 'phone'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
	//	$rules[] = $ShortStringItems;		
		return $rules;	
		
		
		
		
		/*
        return [
            [['title','owner', 'body', 'category', 'author', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
		
		*/
		
		/*
		return [
            [['title','body','category','author','status'], 'string','max' => 255],
            [['owner', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
          
		];
		
		*/
		
		
		
		/*
		
		return [
            [['title'],['body'],['category'],['author'],['status'], 'string'],
            [['status', 'owner', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],

        ];

		*/

		
    }

	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
/**
     * Defenition of relation to user table
     */ 	
	

	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
	public function getUserOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
    /**
     * Defenition of relation to status table
     */  
 	
 
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }	
	

	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
			'owner' => 'owner',
        ];
    }
}
